cmake_minimum_required(VERSION 3.13)
project(advcpp)
include(gtest.cmake)

set(CMAKE_CXX_STANDARD 17)
set(LIB_SOURCE_FILES project/lib/some_lib.cpp)

#   libs
add_library(some_lib STATIC ${LIB_SOURCE_FILES})

#   main program
add_executable(advcpp project/src/main.cpp)
target_link_libraries(advcpp some_lib)
target_include_directories(advcpp PUBLIC project/include)


#   tests
#   GTest needs threading support
find_package (Threads)
add_executable(tests_gtest tests/tests.cpp)
target_link_libraries(tests_gtest strings_lib)
target_link_libraries(tests_gtest gtest ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(tests_gtest ${GTEST_LIBRARIES})
